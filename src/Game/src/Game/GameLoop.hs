{-# LANGUAGE BlockArguments #-}
module Game.GameLoop (mainGameLoop) where

import Data.List (intercalate)
import Control.Exception (catch, SomeException)

import Game.Console.GameLoop (gameLoop)
import qualified Game.Console.GameLoopConfig as GLC (GameLoopConfig(..))
import Game.Modes.Play.GameLoop (playGameLoop)
import Game.Modes.Helper.GameLoop (helperGameLoop)
import qualified Game.GameState as GS
import Game.GameState (GameState(..))
import Game.ConsoleMode (consoleMode, printLine)
import Game.Console.ColorPrinter (colorizeTextFromEnumColor, Color(..))
import Database.Reader(minimumLength, maximumLength)
import Game.InputState (InputState(..))
import Game.Modes.GameMode (GameMode(..), Difficulty(..), readGameMode, readDifficulty)
import qualified Game.Modes.Helper.Difficulty as HD (Difficulty(..), readDifficulty, difficultyOptions)
import qualified Game.Modes.Play.Difficulty as PD (Difficulty(..), readDifficulty, difficultyOptions)


mainGameLoop :: () -> IO Bool
mainGameLoop () = do
  let
    gameLoopConfig :: GLC.GameLoopConfig GameState InputState
    gameLoopConfig = GLC.GameLoopConfig
      { GLC.modifyState = modifyState
      , GLC.isSuccessState = isSuccessState
      , GLC.isFailureState = isFailureState
      , GLC.shouldAwaitInput = shouldAwaitInput
      , GLC.messageFactory = messageFactory
      , GLC.getInputState = getInputState
      , GLC.isValidInputState = isValidInputState
      , GLC.isExitInputState = isExitInputState
      , GLC.exitMessage = exitMessage
      , GLC.invalidInputMessageFactory = invalidInputMessageFactory
      , GLC.successMessageFactory = successMessageFactory
      , GLC.failureMessageFactory = failureMessageFactory
      , GLC.consoleMode = consoleMode
      }

  gameLoop initialState gameLoopConfig

initialState :: GameState
initialState = GameState
    { GS.gameMode = NoMode
    , GS.difficulty = NoDifficulty
    , GS.wordLength = -1
    , GS.inputState = ReadingGameMode
    }

modifyState :: GameState -> String -> IO GameState
modifyState state input = do
  case inputState state of
    ReadingGameMode -> do
      let gameMode = readGameMode input :: GameMode
      return $ state { gameMode = gameMode, inputState = ReadingDifficulty }
    ReadingDifficulty -> do
      let difficulty = readDifficulty input :: Difficulty
      return $ state { difficulty = difficulty, inputState = ReadingWordLength }
    ReadingWordLength -> do
      let wordLength = read input :: Int
      return $ state { wordLength = wordLength, inputState = ReadyToPlay }
    ReadyToPlay -> do
      case gameMode state of
        Play -> do
          let currentDifficulty = generalToPlayDifficulty $ difficulty state
          playGameLoop (wordLength state) currentDifficulty
        Helper -> do
          let currentDifficulty = generalToHelperDifficulty $ difficulty state
          helperGameLoop (wordLength state) currentDifficulty
        NoMode -> error "Invalid game mode."
      return $ state { inputState = ReadingSameGameModeDifficultyAnswer }
    ReadingSameGameModeDifficultyAnswer -> do
      let answer = input
      if answer == "y" || answer == ""
        then return $ state { inputState = ReadingSameWordLengthAnswer }
        else return $ GameState
               { gameMode = NoMode
               , difficulty = NoDifficulty
               , wordLength = -1
               , inputState = ReadingGameMode }
    ReadingSameWordLengthAnswer -> do
      let answer = input
      if answer == "y" || answer == ""
        then return $ state { inputState = ReadyToPlay }
        else return $ state { wordLength = -1, inputState = ReadingWordLength }
    _ -> error "Invalid input state."

generalToPlayDifficulty :: Difficulty -> PD.Difficulty
generalToPlayDifficulty difficulty = case difficulty of
  Easy -> PD.Easy
  Standard -> PD.Standard
  Expert -> PD.Expert
  _ -> error "Invalid play difficulty."

generalToHelperDifficulty :: Difficulty -> HD.Difficulty
generalToHelperDifficulty difficulty = case difficulty of
  Standard -> HD.Standard
  Expert -> HD.Expert
  _ -> error "Invalid helper difficulty."

isSuccessState :: GameState -> Bool
isSuccessState _ = False

isFailureState :: GameState -> Bool
isFailureState _ = False

shouldAwaitInput :: GameState -> Bool
shouldAwaitInput state = case (inputState state) of
  ReadyToPlay -> False
  _ -> True

messageFactory :: GameState -> IO String
messageFactory state =
  case (inputState state) of
    ReadingGameMode -> do
      return $ "Choose game mode (" ++ (intercalate ", " $ map show [Play, Helper]) ++ "): "
    ReadingDifficulty -> do
      return $ "Choose difficulty (" ++ (intercalate ", " $ (case (gameMode state) of
        Play -> PD.difficultyOptions
        Helper -> HD.difficultyOptions)) ++ "): "
    ReadingWordLength -> do
      return $ "Choose word length range [" ++ minimumLength ++ ", " ++ maximumLength ++ "]: "
    ReadingSameGameModeDifficultyAnswer -> do
      return $ "Do you want to play again with the same game mode and difficulty? (Y/n): "
    ReadingSameWordLengthAnswer -> do
      return $ "Do you want to play again with the same word length? (Y/n): "
    _ -> error $ "Cannot create message for input state: " ++ (show $ inputState state)

getInputState :: String -> GameState -> IO InputState
getInputState input state = do
  if input == "" && (inputState state) == ReadyToPlay
    then return $ inputState state
  else if input == exitWord
    then return Exit
  else case (inputState state) of
    ReadingWordLength -> do
      let wordLength = safeRead input 1 :: Int
      if (read minimumLength :: Int) > wordLength || (read maximumLength :: Int) < wordLength
        then return InvalidWordLength
        else return ReadingWordLength
    ReadingSameGameModeDifficultyAnswer -> do
      let answer = input
      if answer == "y" || answer == "n" || answer == ""
        then return ReadingSameGameModeDifficultyAnswer
        else return InvalidSameGameModeDifficultyAnswer
    ReadingSameWordLengthAnswer -> do
      let answer = input
      if answer == "y" || answer == "n" || answer == ""
        then return ReadingSameWordLengthAnswer
        else return InvalidSameWordLengthAnswer
    ReadingGameMode -> do
      let gameMode = readGameMode input
      if gameMode `elem` [Play, Helper]
        then return ReadingGameMode
        else return InvalidGameMode
    ReadingDifficulty -> do
      let difficulty = readDifficulty input
      if (((difficulty == Easy) && ((gameMode state) == Helper))
        || (not $ elem difficulty [Easy, Standard, Expert]))
        then return InvalidDifficulty
        else return ReadingDifficulty
    _ -> return InvalidSameGameModeDifficultyAnswer

safeRead :: Read a => String -> a -> a
safeRead s defultValue = case reads s of
  [(x, "")] -> x
  _         -> defultValue

exitWord :: String
exitWord = "exit"

exitMessage :: String
exitMessage = "Goodbye!..."

isValidInputState :: InputState -> Bool
isValidInputState state = case state of
  InvalidGameMode -> False
  InvalidDifficulty -> False
  InvalidWordLength -> False
  InvalidSameGameModeDifficultyAnswer -> False
  InvalidSameWordLengthAnswer -> False
  _ -> True

isExitInputState :: InputState -> Bool
isExitInputState = (==Exit)

invalidInputMessageFactory :: String -> InputState -> GameState -> IO String
invalidInputMessageFactory _ inputState state =
  case inputState of
    InvalidWordLength ->
      return $ "Invalid input: Word length must be in [" ++ minimumLength
        ++ ", " ++ maximumLength ++ "]"
    InvalidDifficulty ->
      return $ "Invalid input: Difficulty must be in [" ++ (intercalate ", " $ (case (gameMode state) of
        Play -> PD.difficultyOptions
        Helper -> HD.difficultyOptions)) ++ "]"
    InvalidGameMode ->
      return $ "Invalid input: Game mode must be in [" ++ (intercalate ", " $ map show [Play, Helper]) ++ "]"
    InvalidSameGameModeDifficultyAnswer ->
      return "Invalid input: Answer must be 'y' or 'n'"
    InvalidSameWordLengthAnswer ->
      return "Invalid input: Answer must be 'y' or 'n'"
    _ -> error $ "Cannot create invalid input message for input state: " ++ (show inputState)

successMessageFactory :: GameState -> IO String
successMessageFactory state = return ""

failureMessageFactory :: GameState -> IO String
failureMessageFactory state = return ""
