module Game.GameState (GameState(..)) where

import Game.Modes.GameMode (GameMode(..), Difficulty(..))
import Game.InputState (InputState(..))


data GameState = GameState
  { gameMode :: GameMode
  , difficulty :: Difficulty
  , wordLength :: Int
  , inputState :: InputState
  } deriving (Show, Eq)
