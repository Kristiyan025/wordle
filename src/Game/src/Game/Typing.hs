module Game.Typing (MatchedGuess, MatchedGuesses) where


type MatchedGuess = (String, String)
type MatchedGuesses = [MatchedGuess]
