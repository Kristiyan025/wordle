module Game.InputState (InputState(..)) where


data InputState =
    ReadingGameMode -- Initial
  | ReadingDifficulty
  | ReadingWordLength
  | ReadyToPlay
  | ReadingSameGameModeDifficultyAnswer
  | ReadingSameWordLengthAnswer
  | InvalidWordLength
  | InvalidDifficulty
  | InvalidGameMode
  | InvalidSameGameModeDifficultyAnswer
  | InvalidSameWordLengthAnswer
  | Exit
  deriving (Show, Eq)
