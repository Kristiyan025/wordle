module Game.Modes.Helper.InputState (InputState(..), isMatch, convertToMatchSequence, matchAliases) where

import qualified Data.Map as Map

import Game.Modes.Play.InputState (InputState(..))
import Game.GameColors (green, yellow, gray, invalid)
import Game.StringUtils (splitAndFilter)


isMatch :: String -> Bool
isMatch [] = True
isMatch (x:xs) = x `elem` matchLetters && isMatch xs

convertToMatchSequence :: String -> String
convertToMatchSequence inputLine = map transformWord $ splitAndFilter inputLine

transformWord :: String -> Char
transformWord c = case Map.lookup c wordToMatchMap of
  Just value -> value
  Nothing -> invalid

wordToMatchMap :: Map.Map String Char
wordToMatchMap = Map.fromList
  [ ("green", green)
  , ("yellow", yellow)
  , ("gray", gray)
  ]

matchAliases :: [String]
matchAliases = Map.keys wordToMatchMap

matchLetters:: [Char]
matchLetters = Map.elems wordToMatchMap
