module Game.Modes.Helper.Standard.GameLoop (helperStandardGameLoop) where

import Data.List (intercalate)

import Game.Console.GameLoop (gameLoop)
import qualified Game.Console.GameLoopConfig as GLC (GameLoopConfig(..))
import qualified Game.Modes.Helper.Standard.GameState as GS
import Game.Modes.Helper.Standard.GameState (GameState(..), lastGuess, lastMatch)
import Game.Modes.Helper.Standard.NextGuess (filterSet, makeNextGuess)
import Game.Modes.Helper.ConsoleMode (consoleMode, printLine)
import Game.Modes.Helper.InputState (InputState(..), isMatch, convertToMatchSequence, matchAliases)
import Game.Modes.ExitKeywords (exitWord, exitMessage)
import Game.GameColors (green, yellow, gray)
import qualified Game.Console.GameStatePrinter as GSP (printGameState)
import Game.Console.ColorPrinter (colorizeTextFromEnumColor, Color(..))
import Database.Reader (readSmallWordSetWithLength)
import Game.StringUtils (formatOrdinalNumber, isLowerEnglishLettersOnly)


helperStandardGameLoop :: Int -> IO Bool
helperStandardGameLoop wordLength = do
  initialState <- initialStateFactory wordLength
  let
    gameLoopConfig :: GLC.GameLoopConfig GameState InputState
    gameLoopConfig = GLC.GameLoopConfig
      { GLC.modifyState = modifyState
      , GLC.isSuccessState = isSuccessState
      , GLC.isFailureState = isFailureState
      , GLC.shouldAwaitInput = const True
      , GLC.messageFactory = messageFactory
      , GLC.getInputState = getInputState
      , GLC.isValidInputState = isValidInputState
      , GLC.isExitInputState = isExitInputState
      , GLC.exitMessage = exitMessage
      , GLC.invalidInputMessageFactory = invalidInputMessageFactory
      , GLC.successMessageFactory = successMessageFactory
      , GLC.failureMessageFactory = failureMessageFactory
      , GLC.consoleMode = consoleMode
      }

  gameLoop initialState gameLoopConfig

initialStateFactory :: Int -> IO GameState
initialStateFactory wordLength = do
  wordsDB <- readSmallWordSetWithLength wordLength
  let nextGuess = makeNextGuess wordsDB :: String

  return GameState
    { GS.possibleWords = wordsDB
    , GS.guesses  = []
    , GS.nextGuess = nextGuess
    , GS.currentGuesses = 1
    , GS.wordLength = wordLength
    }

modifyState :: GameState -> String -> IO GameState
modifyState state input = do
  let match = convertToMatchSequence input
  let guessWord = GS.nextGuess state
  let newPossibleWords = filterSet guessWord match $ possibleWords state :: [String]
  let nextGuess = makeNextGuess newPossibleWords :: String

  return $ state
    { GS.possibleWords = newPossibleWords
    , GS.guesses = (guessWord, match) : (guesses state)
    , GS.nextGuess = nextGuess
    , GS.currentGuesses = (currentGuesses state) + 1
    }

isSuccessState :: GameState -> Bool
isSuccessState state =
  if null $ guesses state
  then False
  else all (==green) (lastMatch state)

isFailureState :: GameState -> Bool
isFailureState state = null (possibleWords state)

messageFactory :: GameState -> IO String
messageFactory state = do
  (if (currentGuesses state) == 1
  then return ()
  else printGameState state)
  printLine $ colorizeTextFromEnumColor Magenta
    $ "My " ++ (formatOrdinalNumber $ (currentGuesses state)) ++ " guess: " ++ (nextGuess state)
  return $ "Please, give me the corrsponding match sequence (use: "
    ++ (intercalate ", " matchAliases) ++ "): "

getInputState :: String -> GameState -> IO InputState
getInputState input state =
  if input == exitWord
    then return Exit
  else let
    match = convertToMatchSequence input :: String
    in if (length match) /= (wordLength state)
    then return InvalidLength
    else if not $ isMatch $ match
      then return InvalidCharacters
      else return Valid

isValidInputState :: InputState -> Bool
isValidInputState = (==Valid)

isExitInputState :: InputState -> Bool
isExitInputState = (==Exit)

invalidInputMessageFactory :: String -> InputState -> GameState -> IO String
invalidInputMessageFactory _ inputState state =
  case inputState of
    InvalidLength ->
      return $ "Invalid input: Match sequence length does not match secret word length: "
        ++ (show $ wordLength state)
    InvalidCharacters ->
      return "Invalid input: Match sequence contains illegal symbols"

successMessageFactory :: GameState -> IO String
successMessageFactory state = do
  printGameState state
  return $ "I won! I guessed the secret word was indeed: " ++ (colorizeTextFromEnumColor Cyan $ lastGuess state)
    ++ (colorizeTextFromEnumColor Green $ ". I guessed it in " ++ (show $ currentGuesses state) ++ " tries.")

failureMessageFactory :: GameState -> IO String
failureMessageFactory state = do
  return "I couldn't guess the secret word. It's not in my dictionary."

printGameState :: GameState -> IO ()
printGameState state = GSP.printGameState (wordLength state) (guesses state) consoleMode
