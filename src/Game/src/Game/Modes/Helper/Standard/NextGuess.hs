module Game.Modes.Helper.Standard.NextGuess
  (filterSet
  , makeNextGuess
  , calculateScoreForWords
  , argMaxBy
  , boolToInt) where

import Data.List (maximumBy)

import Game.WordMatcher (matchWord, doesWordObeyMatching)


filterSet :: String -> String -> [String] -> [String]
filterSet guessWord matchWord possibleWords =
  filter (\word -> doesWordObeyMatching word matchWord guessWord) possibleWords

makeNextGuess :: [String] -> String
makeNextGuess possibleWords =
  fst $ argMaxBy snd $ zip possibleWords $ calculateScoreForAllWords possibleWords

argMaxBy :: Ord b => (a -> b) -> [a] -> a
argMaxBy f xs = maximumBy (\x y -> compare (f x) (f y)) xs

calculateScoreForAllWords :: [String] -> [Int]
calculateScoreForAllWords possibleWords = calculateScoreForWords possibleWords possibleWords possibleWords

calculateScoreForWords :: [String] -> [String] -> [String] -> [Int]
calculateScoreForWords [] _ _ = []
calculateScoreForWords (word:ws) secretWords possibleWords =
  (calculateScoreForWord word secretWords possibleWords)
  : (calculateScoreForWords ws secretWords possibleWords)

calculateScoreForWord :: String -> [String] -> [String] -> Int
calculateScoreForWord _ [] _ = 0
calculateScoreForWord word (secretWord:ss) possibleWords =
  (calculateScoreForSecret word secretWord possibleWords)
  + (calculateScoreForWord word ss possibleWords)

calculateScoreForSecret :: String -> String -> [String] -> Int
calculateScoreForSecret guessWord secretWord possibleWords = let
    match :: String
    match = matchWord guessWord secretWord

    filteredPossibleWords :: Int
    filteredPossibleWords = filterCount (\word -> not $ doesWordObeyMatching word match guessWord) possibleWords
    in filteredPossibleWords

filterCount :: (a -> Bool) -> [a] -> Int
filterCount _ [] = 0
filterCount predicate (x:xs) = (boolToInt $ predicate x) + filterCount predicate xs

boolToInt :: Bool -> Int
boolToInt b = fromEnum b
