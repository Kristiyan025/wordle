module Game.Modes.Play.ConsoleMode (consoleMode, consolePrefix, printStr, printLine) where

import qualified Game.Console.ConsoleMode as CM (ConsoleMode(..), getConsolePrefix, printStr, printLine)


consoleMode :: CM.ConsoleMode
consoleMode = CM.SubConsole

consolePrefix :: String
consolePrefix = CM.getConsolePrefix consoleMode

printStr :: String -> IO ()
printStr line = CM.printStr consoleMode line

printLine :: String -> IO ()
printLine line = CM.printLine consoleMode line