module Game.Modes.Play.InputState (InputState(..)) where


data InputState =
    Valid
  | InvalidLength
  | InvalidCharacters
  | Exit
  deriving (Show, Eq)
