module Database.Hydrator (hydrateDatabase, hydrateDatabaseFile) where

import Text.Printf (printf)
import Data.Time.Clock (getCurrentTime)
import System.Process (spawnProcess, waitForProcess, ProcessHandle)
import System.Environment (getExecutablePath)
import System.FilePath ((</>), takeDirectory)

import Database.Hydration.WordPagesFlattener (wordLengths)
import Database.Hydration.DBShardCreator (calculateElapsedTime)


processName :: String
processName = "DBShardProcessMain"

getProcessExecutablePath :: IO FilePath
getProcessExecutablePath = do
  executablePath <- getExecutablePath
  let processExecutable = (takeDirectory executablePath) </>
                          ".." </> ".." </> ".." </> processName </> "build" </> processName </> processName
  return processExecutable

spawnProcessForWordLength :: Int -> Bool -> IO ProcessHandle
spawnProcessForWordLength wordLength printDebugInfo = do
  processExecutable <- getProcessExecutablePath
  spawnProcess processExecutable [(show wordLength), (show printDebugInfo)]

runInParallel :: [Int] -> Bool -> IO ()
runInParallel wordLengths printDebugInfo = do
    processHandles <- mapM (\(a, b) -> spawnProcessForWordLength a b) [(wordLengths, printDebugInfo) | wordLengths <- wordLengths]
    results <- mapM waitForProcess processHandles
    return ()

processBatch :: Int -> [a] -> Bool -> ([a] -> Bool -> IO ()) -> IO ()
processBatch _ [] _ _ = return ()
processBatch batchSize input printDebugInfo action = do
  let (batch, remaining) = splitAt batchSize input
  action batch printDebugInfo
  processBatch batchSize remaining printDebugInfo action

hydrateDatabase :: Bool -> IO ()
hydrateDatabase printDebugInfo = do
  putStrLn "Starting database hydration..."
  start <- getCurrentTime
  processBatch 2 wordLengths printDebugInfo runInParallel
  end <- getCurrentTime
  let elapsedTime = calculateElapsedTime start end :: Double
  putStrLn $ "Finished database hydration. Took " ++ (printf "%.2f" elapsedTime) ++ " seconds"

hydrateDatabaseFile :: Int -> Bool -> IO ()
hydrateDatabaseFile wordLength printDebugInfo = do
  putStrLn $ "Starting database hydration for words with length " ++ (show wordLength) ++ "..."
  start <- getCurrentTime
  runInParallel [wordLength] printDebugInfo
  end <- getCurrentTime
  let elapsedTime = calculateElapsedTime start end :: Double
  putStrLn $ "Finished database hydration. Took " ++ (printf "%.2f" elapsedTime) ++ " seconds"

