module Main where

import System.Environment

import Database.Hydrator (hydrateDatabase)


main :: IO ()
main = do
    args <- getArgs
    let argMap = parseArgs args :: [(String, String)]
    let printDebugInfo = False
    if (length argMap) == 1 && (fst $ head argMap) == "print-debug-info"
        then do
          let [(_, value)] = argMap
          let printDebugInfo = read value :: Bool
          hydrateDatabase printDebugInfo
        else do
          hydrateDatabase False

parseArgs :: [String] -> [(String, String)]
parseArgs = map parseArg
  where
    parseArg arg = case break (== '=') arg of
        (key, _:value) -> (key, value)
        _              -> error $ "Invalid argument format: " ++ arg
