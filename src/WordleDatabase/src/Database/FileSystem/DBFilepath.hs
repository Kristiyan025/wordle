module Database.FileSystem.DBFilepath (getMainDbFilePath, getSmallDbFilePath) where

import System.FilePath ((</>))
import Text.Printf (printf)

import Database.FileSystem.ProjectRootFinder (findProjectRoot)


getMainDbFilePath :: Int -> IO FilePath
getMainDbFilePath wordLength = do
  baseFilePath <- getDbBaseFilePath ()
  baseFileName <- getDbBaseFileName wordLength
  return $ baseFilePath </> baseFileName ++ dbFileExtension

getSmallDbFilePath :: Int -> IO FilePath
getSmallDbFilePath wordLength = do
  baseFilePath <- getDbBaseFilePath ()
  baseFileName <- getDbBaseFileName wordLength
  return $ baseFilePath </> baseFileName ++ "_small" ++ dbFileExtension

dbFileExtension :: String
dbFileExtension = ".db"

getDbBaseFileName :: Int -> IO String
getDbBaseFileName wordLength = do
  projectRoot <- findProjectRoot
  return $ "words" ++ (printf "%02d" wordLength)

getDbBaseFilePath :: () -> IO FilePath
getDbBaseFilePath () = do
  projectRoot <- findProjectRoot
  return $ projectRoot </> "database"
