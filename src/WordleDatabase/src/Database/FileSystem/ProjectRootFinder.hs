module Database.FileSystem.ProjectRootFinder (findProjectRoot) where

import System.Directory (getCurrentDirectory, doesFileExist, getDirectoryContents)
import System.FilePath ((</>), takeDirectory)

markerFile :: String
markerFile = "cabal.project" -- The config file for the project

findProjectRoot :: IO FilePath
findProjectRoot = do
    currentDir <- getCurrentDirectory
    maybeRoot <- findMarkerFileInAncestors currentDir
    case maybeRoot of
            Just root -> return root
            Nothing   -> error "Unable to find project root."

findMarkerFileInAncestors :: FilePath -> IO (Maybe FilePath)
findMarkerFileInAncestors dir = do
    let markerFilepath = dir </> markerFile
    exists <- doesFileExist markerFilepath
    if exists
        then return (Just dir)
        else if dir == "/"  -- reached the root without finding the marker file
                then return Nothing
                else findMarkerFileInAncestors (takeDirectory dir)
