module Main where

import System.Environment (getArgs)

import Database.Hydration.DBShardCreator (createMainDBFile, createSmallDBFile)


main :: IO ()
main = do
  [firstArg, secondArg] <- getArgs
  let wordLength = read firstArg :: Int
  let printDebugInfo = read secondArg :: Bool
  createMainDBFile wordLength printDebugInfo
  createSmallDBFile wordLength printDebugInfo
